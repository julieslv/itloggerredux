import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addTech } from '../../actions/techActions';
import M from 'materialize-css/dist/js/materialize';

const AddTechModal = ({ addTech }) => {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');

	const onSubmit = () => {
		if (firstName === '' || lastName === '') {
			M.toast({ html: 'Please enter a first and last name' });
		} else {
			addTech({
				firstName,
				lastName,
			});
			M.toast({ html: `${firstName} ${lastName} was added as a tech.` });
			//clear flields
			setFirstName('');
			setLastName('');
		}
	};

	return (
		<div id='add-tech-modal' className='modal'>
			<div className='modal-content'>
				<h4>New technician</h4>
				<div className='input-field'>
					<input
						type='text'
						name='firstName'
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
					/>
					<label htmlFor='firstName' className='active'>
						First name
					</label>
				</div>
				<div className='input-field'>
					<input
						type='text'
						name='lastName'
						value={lastName}
						onChange={e => setLastName(e.target.value)}
					/>
					<label htmlFor='firstName' className='active'>
						Last name
					</label>
				</div>
				<div className='modal-footer'>
					<button
						onClick={onSubmit}
						className='modal-close waves-effect waves-teal btn'>
						Enter
					</button>
				</div>
			</div>
		</div>
	);
};

AddTechModal.propTypes = {
	addTech: PropTypes.func.isRequired,
};
export default connect(null, { addTech })(AddTechModal);
