import { combineReducers } from 'redux';
import logReducer from './logReducer';
import techReducer from './techReducer';

export default combineReducers({
	// what we are calling our state : where is the state
	log: logReducer,
	tech: techReducer,
});
